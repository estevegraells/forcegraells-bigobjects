# README #

Este REPO corresponde al articulo publicado en el blog [forcegraells.com](forcegraells.com) referente a Big Objects: [ http://forcegraells.com/2017/11/04/salesforce-big-objects/]( http://forcegraells.com/2017/11/04/salesforce-big-objects/).

El único cambio necesario es proporcionar las credenciales (username/password) en el fichero **process-conf.xml** en la carpeta */CargaCiudadnos/config* .

Para generar el password encriptado, consultar este artículo [https://developer.salesforce.com/docs/atlas.en-us.dataLoader.meta/dataLoader/command_line_create_encryption_key.htm](https://developer.salesforce.com/docs/atlas.en-us.dataLoader.meta/dataLoader/command_line_create_encryption_key.htm).

Cualquier duda o consulta: esteve.graells@gmail.com (prometo responder :-) ).